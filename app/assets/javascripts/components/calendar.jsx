import React from 'react';
import moment from 'moment';

const { object } = React.PropTypes;

const dateFormat = 'MMMM D, YYYY';

class Calendar extends React.Component {

  static PropTypes = {
    startDate: object,
    endDate: object
  };

  formattedStartDate = () => {
    return moment(this.props.startDate).format(dateFormat);
  };

  formattedEndDate = () => {
    return moment(this.props.endDate).format(dateFormat);
  };

  render() {
    return(
      <section id="calendar">
        <h1>Week of {this.formattedStartDate()} to {this.formattedEndDate()}</h1>
      </section>
    );
  }
}

export default Calendar
