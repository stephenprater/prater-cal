import React from 'react';
import ReactDOM from 'react-dom';
import TestUtils from 'react-addons-test-utils';
import Calendar from '../calendar.jsx';

describe('Calendar', () => {
  let component, node;

  beforeEach(() => {
    let startDate, endDate;

    startDate = new Date(2015, 3, 10, 0, 0);
    endDate = new Date(2015, 3, 16, 0, 0);

    component = TestUtils.renderIntoDocument(
      <Calendar startDate={startDate} endDate={endDate} />
    );
    node = ReactDOM.findDOMNode(component);
  });

  it('renders the calendar day', () => {
    expect(node.innerText).toBe('Week of April 10, 2015 to April 16, 2015')
  });
});
