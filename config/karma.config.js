var webpackConfig = require('./webpack.config');

webpackConfig.watch = true;

module.exports = function(config) {
  config.set({

    autoWatchBatchDelay: 400,

    browsers: ['PhantomJS'],

    files: [
      '../scripts/karma/polyfill.js',
      {
        pattern: '../app/assets/javascripts/**/*-test.js*',
        watched: false,
        included: true,
        served: true
      }
    ],

    frameworks: [
      'jasmine-ajax',
      'jasmine'
    ],

    preprocessors: {
      '../app/assets/javascripts/**/*-test.js*': ['webpack']
    },

    webpack: webpackConfig,

    webpackMiddleware: {
      noInfo: true
    },

    plugins: [
      require('karma-jasmine-ajax'),
      require('karma-jasmine'),
      require('karma-webpack'),
      require('karma-phantomjs-launcher')
    ]

  });
};
