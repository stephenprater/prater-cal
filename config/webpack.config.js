var resolveRoot = require('path').resolve.bind(null, __dirname, '..');
var webpack = require('webpack');
var ExtractTextPlugin = require('extract-text-webpack-plugin');
var StatsPlugin = require('stats-webpack-plugin');
var isProduction = process.env.NODE_ENV === 'production';

var config = {
  entry: {
    app: resolveRoot('./app/assets/javascripts/app.jsx')
  },

  output: {
    path: resolveRoot('./public/webpack'),
    filename: isProduction ? '[name]-[chunkhash].js' : '[name].js'
  },

  resolve: {
    root: resolveRoot()
  },

  module: {
    loaders: [
      {
        test: /\.json$/,
        loader: 'json'
      },
      {
        test: /\.jsx?$/,
        loader: 'babel',
        exclude: /node_modules/
      },
      {
        test: /\.scss$/,
        loader: ExtractTextPlugin.extract('style-loader', 'css-loader!sass-loader' +
                                          '?includePaths[]=' + resolveRoot('./app/assets/stylesheets') +
                                            '&includePaths[]=' + require('bourbon').includePaths +
                                              '&includePaths[]=' + resolveRoot('./node_modules/bourbon-neat/app/assets/stylesheets')
                                         )
      },
      {
        test: /\.css$/,
        loader: 'style!css'
      }
    ]
  },

  plugins: [
    new ExtractTextPlugin(isProduction ? '[name]-[chunkhash].css' : '[name].css'),
    new StatsPlugin('manifest.json', {
      chunkModules: false,
      source: false,
      chunks: false,
      modules: false,
      assets: true
    })
  ]

};


if(isProduction) {
  config.plugins.push(
    new webpack.optimize.UglifyJsPlugin({
      compressor: { warnings: false },
      sourceMap: false
    }),
    new webpack.DefinePlugin({
      'process.env': { NODE_ENV: JSON.stringify('production') }
    }),
    new webpack.optimize.DedupePlugin(),
    new webpack.optimize.OccurenceOrderPlugin()
  );
}

module.exports = config;
